﻿CREATE PROCEDURE uspGetUser
(
	@Command VARCHAR(MAX),

	@UserId NUMERIC(18,0),
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

	IF @Command='SELECT'
	BEGIN

		BEGIN TRANSACTION

		BEGIN TRY

			SELECT U.UserId,
			U.FirstName,
			U.LastName
				FROM tblUser AS U
					WHERE U.FirstName=@FirstName OR U.LastName=@LastName

			SET @Status=1
			SET @Message='SELECT SUCCESSFULL'

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			
			SET @ErrorMessage=ERROR_MESSAGE()
			
			SET @Status=0
			SET @Message='SELECT EXCEPTION'
			ROLLBACK TRANSACTION

			RAISERROR(@ErrorMessage,16,1)

		END CATCH

	END
	

	END
