﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertPage.aspx.cs" Inherits="Sol_CheckBox_List.InsertPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>

                <table>

                    <tr>
                        <td> 
                            <asp:TextBox ID="txtFirstName" runat="server" placeholder="FirstName"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server" placeholder="LastName"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:CheckBoxList ID="chkHobbies" runat="server">

                                <asp:ListItem Value="1" Text="Drawing"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Reading Novels"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Listening Music"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Gardening"></asp:ListItem>
                                <asp:ListItem Value="5" Text="Cricket"></asp:ListItem>

                            </asp:CheckBoxList>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </td>
                    </tr>

                </table>

            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
