﻿using Sol_CheckBox_List.DAL;
using Sol_CheckBox_List.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_CheckBox_List
{
    public partial class InsertPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            this.AddUserData();
        }

        private void AddUserData()
        {
            UserEntity userEntityObj = new UserEntity()
            {
                FirstName=txtFirstName.Text,
                LastName=txtLastName.Text
            };

            string message=new UserDal().SetUserData(userEntityObj);

            //get UserId
            var valueId= new UserDal().GetUserData(userEntityObj);
           
            //lblMessage.Text = chkHobbies.SelectedItem.Text;


            foreach (ListItem val in chkHobbies.Items)
            {
                if (val.Selected == true)
                {
                    List<HobbyEntity> hobbyEntityObj = new List<HobbyEntity>()
                    {
                        new HobbyEntity()
                        {
                            HobbyName=chkHobbies.SelectedItem.Text,
                            Interested=Convert.ToBoolean(chkHobbies.SelectedItem),
                            UserId=valueId
                        }
                    };

                    string messageResult = new UserDal().SetHobbiesData(hobbyEntityObj);

                    lblMessage.Text = messageResult;
                }
            }

        }
    }
}