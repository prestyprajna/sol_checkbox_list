﻿CREATE PROCEDURE uspSetUser
(
	@Command VARCHAR(MAX),

	@UserId NUMERIC(18,0),
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

	IF @Command='INSERT'
	BEGIN

		BEGIN TRANSACTION

		BEGIN TRY

			INSERT INTO tblUser
			(
			FirstName,
			LastName
			)
			VALUES
			(
			@FirstName,
			@LastName
			)

			SET @Status=1
			SET @Message='INSERT SUCCESSFULL'

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			
			SET @ErrorMessage=ERROR_MESSAGE()
			
			SET @Status=0
			SET @Message='INSERT EXCEPTION'
			ROLLBACK TRANSACTION

			RAISERROR(@ErrorMessage,16,1)

		END CATCH

	END
	

	END
