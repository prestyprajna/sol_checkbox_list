﻿using Sol_CheckBox_List.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_CheckBox_List.DAL
{
    public class UserDal
    {
        #region  declaration
        private UserDCDataContext _dc = null;
        #endregion
          
        #region  constructor
        public UserDal()
        {
            _dc = new UserDCDataContext();
        }

        #endregion

        #region  public methods

        public string SetUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            _dc?.uspSetUser(
                "INSERT",
                userEntityObj?.UserId,
                userEntityObj?.FirstName,
                userEntityObj?.LastName,
                ref status,
                ref message
                );

            return message;

        }

        public decimal GetUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            var getQuery = _dc?.uspGetUser(
                "SELECT",
                userEntityObj?.UserId,
                userEntityObj?.FirstName,
                userEntityObj?.LastName,
                ref status,
                ref message
                )
                ?.AsEnumerable()
                ?.Select((leUserObj) => new UserEntity()
                {
                    UserId = leUserObj?.UserId
                })
                ?.FirstOrDefault();

            return Convert.ToDecimal(getQuery?.UserId);

        }

        public string SetHobbiesData(List<HobbyEntity> hobbyEntityObj)
        {
            int? status = null;
            string message = null;

            foreach(HobbyEntity val in hobbyEntityObj)
            {
                _dc?.uspSetHobbies(
               "INSERT",
               val?.HobbyId,
               val?.HobbyName,
               val?.Interested,
               val?.UserId,
               ref status,
               ref message
               );
            }           

            return message;

        }


        #endregion
    }
}