﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_CheckBox_List.Models
{
    public class HobbyEntity
    {
        public decimal? HobbyId { get; set; }

        public string HobbyName { get; set; }

        public Boolean Interested { get; set; }

        public decimal? UserId { get; set; }

    }
}